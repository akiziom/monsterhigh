$(document).ready(function() {

	$('.page-title .title-line').css({left:$('.page-title .page-title-text').width()+10+'px'});
	$('.page-title-1 .title-line-1').css({left:$('.page-title-1 .page-title-text-1').width()+10+'px'});
	$('.page-title-2 .title-line-2').css({left:$('.page-title-2 .page-title-text-2').width()+10+'px'});


	$('.footer_menu_off').click(function(e){
		e.preventDefault();
		$('.footer_menu').slideToggle(200);
	});

	$('.menu_off').click(function(e){
		e.preventDefault();
		$('.small_menu').slideToggle(200);
	});

	$('.all_dolls_left').click(function(e){
		e.preventDefault();
		$('.left_list').slideToggle(200);
	});

	$('.all_dolls_right').click(function(e){
		e.preventDefault();
		$('.right_list').slideToggle(200);
	});


	$(".lightSlider").lightSlider({
		item:3,
		slideMargin: 0,
		pager: false,
		loop: true,
		responsive:[
		{
			breakpoint:1200,
			settings: {
				item:2
			}
		},{
			breakpoint:480,
			settings: {
				item:1
			}
		}
		]
	});


	$(".lightSlider_1").lightSlider({
		item:2,
		slideMargin: 0,
		pager: false,
		loop: true,
	});


	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		dots: false,
		focusOnSelect: true
	});
});












